﻿using Microsoft.AspNetCore.Mvc;
using MyRazorPage.Models;
using Newtonsoft.Json;
using System.Text;

namespace MyRazorPage.Controllers
{
    public class BranchesController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:7218/api");
        private readonly HttpClient _client;
        public BranchesController()
        {
            _client = new HttpClient();
            _client.BaseAddress = baseAddress;
        }
        [HttpGet]
        public IActionResult Index()
        {
            List<BranchViewModel> list = new List<BranchViewModel>();
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + "/Branches/GetBranches").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                list = JsonConvert.DeserializeObject<List<BranchViewModel>>(data);
            }
            return View(list);
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(BranchViewModel branch)
        {
            try
            {
                string data = JsonConvert.SerializeObject(branch);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _client.PostAsync(_client.BaseAddress + "/Branches/PostBranch", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Created!";
                    return RedirectToAction("Index");
                }
                
                
            }
            catch(Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
            return View();
        }
        [HttpGet]
        public IActionResult Edit(int id)
        {
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + $"/Branches/GetBranch/{id}").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                var branch = JsonConvert.DeserializeObject<BranchViewModel>(data);
                return View(branch);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public IActionResult Edit(int id, BranchViewModel branch) 
        {
            if (ModelState.IsValid)
            {
                HttpResponseMessage response = _client.PutAsJsonAsync(_client.BaseAddress + $"/Branches/PutBranch/{id}", branch).Result;
                if(response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Lỗi khi cập nhật");
                }
            }
            return View(branch);
        }
        [HttpGet]
        public IActionResult Delete(int id)
        {
            HttpResponseMessage response = _client.GetAsync(_client.BaseAddress + $"/Branches/GetBranch/{id}").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                var branch = JsonConvert.DeserializeObject<BranchViewModel>(data);
                return View(branch);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost,ActionName("Delete")]
        public IActionResult DeleteConFirmed(int id) 
        {
            HttpResponseMessage response = _client.DeleteAsync(_client.BaseAddress + $"/Branches/DeleteBranch/{id}").Result;
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                Console.WriteLine(response);
                return RedirectToAction("Index");                
            }
        }
    }
}
